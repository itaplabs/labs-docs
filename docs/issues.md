#BitBucket Issues Templates

##New Feature While in QA

- Title: `[QA] <name of feature>`
- Assignee: `<person who made feature>`

```
Changes:
    - Category of Tool -> Name of Tool
        - Changes made go here
        - For example, "reconfigured called to api"

Added:
    - Category of Tool -> Name of Tool
        - Things added (if relevant)

Removed:
    - Category of Tool -> Name of Tool
        - Things removed (if relevant)

Changed Files:
    - dashboard/example.html
    - dashboard/a_folder/another_example.php

Date Pushed to QA: <DATE>
Scheduled Push to Production: <DATE>
```