This tutorial will explain how to set up a git repository for use with our systems, as well as explain how to set up git to automatically push your changes to the WebServices server when you push to the git origin.

If you don't know anything about git, this might be a little confusing. I'll try my best to explain it though.

# Setting up an RSA key for Git

Before we start messing with git at all, you're going to need to set up an RSA keypair to use with SSH. This is so that you don't have to type out your login to the server and to BitBucket every time you push changes. First, let's generate an RSA keypair (run this in the 'git bash' program)

```bash
# Generate RSA Keypair
ssh-keygen -t rsa
```

When you run this command, it will ask you where you'd like to save the key, as well as a passphrase for your key. Just press enter for each of these that come up. We want the key to go to the default location. Also, we definitely don't want to set a passphrase for the key, because the whole point of this is that we want to be able to use ssh without having to type a password.

Once this is done, if you look in `/c/Users/[YOUR_USERNAME]/.ssh/`, you'll see that you now have an `id_rsa` and an `id_rsa.pub` file. Now, you want to put your public key on the server, so that it can authenticate you with your key. This is a very simple process.

```bash
# Move Keypair to server
ssh-copy-id [YOUR_USERNAME]@ldvwebapa02.www.purdue.edu
```

The command will ask for your password (this will be your career account password) for the server, then it should transfer the public key over to the server. Once it's done, try running `ssh [Your_Username]@ldvwebapa02.www.purdue.edu` -- It should log you in without asking for your password. If that happens, you're good to go on this step.

Next, we're going to have to backup your `.ssh` folder, because the `.ssh` folder is in the C drive, which gets wiped each time you log out. Copy your `.ssh` folder over to your home directory (you'll need to create a folder called `home` in your `W:/pu.data` folder first).

```bash
# Copy your .ssh folder into your W:\ drive so your keys won't be erased when you log out
cp -r /c/Users/[YOUR_USERNAME]/.ssh /w/pu.data/home/
```

Now, you have a copy of your keys and authorized hosts in a place that won't be erased.

!!! note
	If you don't use the exact `/w/pu.data/home/` directory for your .ssh folder, the `setup` script mentioned later won't work for you

## Adding your RSA key to BitBucket

You'll also want to tell BitBucket about your key so that you can push without logging in. Head to your settings on the BitBucket website. Under `SECURITY->SSH keys`, you'll want to click `Add key` and copy-paste the contents of your `id_rsa.pub` into the `Key` section.

# Cloning the repository

Next, you're going to want to clone the relevant repository to somewhere that will be saved when you log off. This means you should not put it anywhere on the C drive. Either put it in your `W:\pu.data\` directory somewhere or on a flash drive. I prefer to use git bash to work with git. Just type "git bash" in the start menu and it should pop up.

```bash
# Navigate to where you want your repository folder to be located
cd /w/pu.data/Desktop/

# Clone the repository - See below for a list of our repositories
git clone {repository URL}

# Now the repository is at /w/pu.data/Desktop/Dashboard/

# Every time you open git bash, you'll want to run this command to tell it where to find your RSA key
# Run this command from one of the git repositories, because they contain the script.
. ./setup

# Some common git commands you'll need to use
git pull # Pulls any changes made by other people from 'origin' (GitHub)
git add . # Stages all changes in the repository
git commit -m "Your Message" # Commits all staged changes. Commit message is mandatory and should summarize what changes you've made since your last commit
git push origin master # Pushes any commits you've made to the master branch to the 'origin' which is GitHub

# I usually just combine these all into one command
git pull && git add . && git commit -m "Your message" && git push origin master
# Don't forget to change the commit message though

# Even better, once you've run the setup script, you'll be able to use the function "gits" which is a shortcut for this.
gits "commit message" # Equivalent to 'git pull && git add . && git commit -m "commit message" && git push origin master'
```

Additionally, I have provided some common commands you'll need when working with git. Now, we want to make it so that any time we push changes to GitHub, they're also pushed automatically to the WebServices server for testing.

# Automatically pushing to the server when you push to origin

Every git repository comes equipped with something called "hooks". These hooks are just scripts or programs that you put in a specific place that git will run when certain things happen. Before we set up any hooks to run, we'll need to add another remote to our repository.

!!! tip "Git Remotes"
	A remote is just a location that is registered with git that contains a copy of the repository. In my example with the dashboard, the remote we currently have is called "origin" and is located on BitBucket's servers. We can create new remotes and work with them whenever we want to.

We'll be making a remote called "dev" which will be linked to the WebServices server. Below is the command to add the remote for the dashboard repository. You can name the remote whatever you want. It doesn't have to be "dev".

```bash
# Add remote called "dev" for the dashboard repository on the server
git remote add dev ssh://[YOUR_USERNAME]@ldvwebapa02.www.purdue.edu:/var/www/html/root/www.purdue.edu/itap/itpm-ls/labs/git-deploy/{repository}.git
```

This command will set up the remote as a connection over ssh. Now, if you ran `git push dev master`, it would connect over ssh to the server and send your latests commits to the server.

The repository you push to on the server is actually not directly the location that will be accessed from the web. This is an empty repository that is set up to put the correct files into the proper location.

For example, the `/labs/git-deploy/dashboard.git` repository will publish changes under the folder `/labs/Dashboard`.

Here's a listing of all the git remotes that are on the server:

Repository Name | Location on Dashboard | Location on BitBucket
--------------- | --------------------- | ---------------------
Dashboard | `/var/www/html/root/www.purdue.edu/itap/itpm-ls/labs/git-deploy/dashboard.git` | `git@bitbucket.org:itaplabs/dashboard.git`
LabsAPI | `/var/www/html/root/www.purdue.edu/itap/itpm-ls/labs/git-deploy/api.git` | `git@bitbucket.org:itaplabs/labsapi.git`
Documentation | N/A | `git@bitbucket.org:itaplabs/api-docs.git`

Now, all that's left is to set up the git hook on your repository to push to dev whenever you push to origin. Navigate to your repository, then go to `.git/hooks` (you might have to show hidden items to see the `.git` folder). Create a file named `pre-push` (no file extension, not `pre-push.sample`) and put the script below in it.

```bash
#Put everything below this into .git/pre-push (NOT INCLUDING THIS LINE!)

#!/bin/bash
remote="$1"
url="$2"

while read local_ref local_sha remote_ref remote_sha
do
	if [ $remote = "origin" ]
	then
		echo "Detected push to origin. Automatically pushing latest commit to dev!"
		cp -r /w/pu.data/home/.ssh /c/Users/[YOUR_USERNAME]/
		git push dev master
	fi
done

exit 0
```

Save the file and you should be good to go! Try pushing new changes to origin (`git push origin master`). You should see a message saying it's pushing your commits to dev and output from two different pushes. Also, you shouldn't need to type in your password. If you do, you've messed something up with the RSA keys.