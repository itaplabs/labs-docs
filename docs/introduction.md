# Introduction

Welcome to The Labs Docs! Here, you will learn how we use the various tools available to us to create beautiful and interactive web pages!

There are examples of code that can be used over to the right. If you don't see an example for something, make sure you're looking at the tab for the language related to the topic you're reading.

## Prerequisites

Before starting on these tutorials, you should understand and be able to use HTML, CSS, JavaScript, SQL, and PHP at least at a basic level.

Additionally, jQuery is a popular library for JavaScript that adds many useful functions and selectors for manipulating the DOM on pages. Also, many of the libraries we use require jQuery.
It is highly recommended you learn how to use jQuery, because it will make your life a lot easier.

- HTML Tutorial - [W3Schools HTML](https://www.w3schools.com/html/default.asp)
- CSS Tutorial - [W3Schools CSS](https://www.w3schools.com/css/default.asp)
- JavaScript Tutorial - [W3Schools JavaScript](https://www.w3schools.com/js/default.asp)
- jQuery Tutorial - [W3Schools jQuery](https://www.w3schools.com/jquery/default.asp) ([Video Tutorial](https://www.youtube.com/watch?v=BWXggB-T1jQ))
- PHP Tutorial - [W3Schools PHP](https://www.w3schools.com/php/php_intro.asp) (Note: we use PHP 5.3 on the ITCR-OPS Server and PHP 5.4 on the WebServices Server)
- SQL Tutorial - [W3Schools SQL](https://www.w3schools.com/sql/default.asp)

!!! note "A note on CSS"
    Some people don't think CSS is very important to know, but the real truth is that nobody likes ugly web pages. If you're going to take the time to create something, don't you want it to be something you're proud of?

    Well-designed web pages do more than just look pretty. They are much easier for people to understand and they are more user friendly. You'll probably get more questions and complaints if your pages don't look nice or don't have a good layout. Nobody wants to spend their whole shift explaining how to use their tool.
    
    Take this page, for example... Open up this webpage in Firefox, hit Alt, and go to `View > Page Style > No Style`. It looks beautiful now, but when you remove CSS, it's ugly and hard to navigate.

