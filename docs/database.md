The database we use here is a MySQL database with a PhpMyAdmin front end that makes administration a bit easier.

## Getting into PhpMyAdmin

The PhpMyAdmin database administration page can be found at [purdue.edu/phpmyadmin](https://purdue.edu/phpmyadmin). The login is `lsops`, but it has a different password from the normal `lsops` login. You can find the password on the passwords page of the wiki. Once you get into PhpMyAdmin, you'll want to select the database to use. The one we use is `lsopskeys` (not `lsopskey`).

Once you're looking at the tables in the database, you can click on one to see its contents. You can navigate to the `Structure` page to look at information about the columns in a table. The SQL tab will allow you to run queries on the database. This can be useful for testing, because often php-mysqli doesn't give much information about what's wrong with your query.

You can find tutorials and more information about PhpMyAdmin all over the web, so if you can't figure out how to do something, just look it up or ask a fellow TSLA.

## Accessing the database from PHP

```php
<?php
    //This is the absolute path, but you can use relative paths for includes as well. 
    require("/var/www/html/root/www.purdue.edu/itap/itpm-ls/labs/api/objects/Database.php");
?>
```

!!! tip "Hold on a second"
    Before talking about how to access the database through PHP directly, I'd like to take a minute to mention to you that we have the Labs API. The API was designed so that information stored in our database can be accessed and modified in any language and without having to write SQL queries.

    This has many advantages, the main one being that we don't have to write everything in PHP. This is good, because code written in PHP can be quite difficult to organize properly and ends up turning into a big mess of spaghetti.

    Using the API also means that your users' inputs will automatically be checked for validity and proper length, as well as sanitized. Because of this, it is MUCH safer to use the API rather than mysqli.

    More information about using the API can be found in the [Labs API](labs_api.md) documentation below.

### The Database Class

Despite the usefulness of the API, there will still be some situations where you need to access the database directly. In this case, you should use the `Database` class found in `/labs/api/objects/Database.php`. You just create a new instance of the class and you will have a database connection ready to go.

```php
<?php
    $db = new Database();
    $response = $db->select("SELECT * FROM lsoemployee_data");
    //$response is now [["user_id" => "Peter Boilermaker", ...], ["user_id" => "Mitch Daniels", ...]]
?>
```

The `Database` class offers some nice utility functions that will save you a lot fo time and space. For example, if you just want to run a standard `SELECT` query, you can use the `Database::select(...)` method and you will get an array of associative arrays, where each array nested inside the first array is a row returned from the database. It may seem complicated, but the example on the right should help clear this up.

The `Database` class has a few other functions that are handy or just make your code more clear. More information can be found in the [Labs API Documentation](#labs-api).

### Directly interfacing with mysqli

If you need to do something that won't work with the functions provided or you just want to interface directly with mysqli, you can access the mysqli object via `Database::conn`. The example below shows how you would make a simple `SELECT` query.

```php
<?php
  $db = new Database();
  $result = $db->conn->query("SELECT * FROM lsoemployee_data");
  //$result is now a mysqli_result object.
  //To get responses, you'd use mysqli_result::fetch_assoc()
  while($row = $result->fetch_assoc()) {
    echo $row;
  }
  //This will echo the associative array corresponding to each row that the query returned.
?>
```

You can find more information about php's mysqli [here](http://php.net/manual/en/book.mysqli.php).

