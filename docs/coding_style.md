# Purpose
	
This coding style guide is to ensure that all of our code for the dashboard looks the same, regardless of who is actually coding. While the syntax of the code will change depending on the language, this style must be kept the same regardless. This guide is still incomplete and can still be up for debate in certain parts of the guide. Note: If you disagree with a rule listed below, you still need to follow it. This is to ensure that all code looks the same, regardless of who wrote the code. 
	
# Variables
	
Every variable should use camelCase when they are named. If a variable is one word, then it remains lowercase. When you are declaring a variable, write a comment that describes the purpose of the variable (This should be done only if the description would be longer than what the variable declaration as a general rule)

	var sith = "Jar Jar" // purpose of variable
	var jediOrder = "Obiwan" // purpose of variable

# Indents

When it is necessary to indent the code, use 1 tab to show that code exists inside the curly brackets.

	function jarJar(){
   		if (person == "Jar Jar") { // 1 tab
        	sith == "Yes" // 2 tabs
    	} // 1 tab
	}

# Whitespace
	
Whitespace, in general, should be minimized whenever possible. For this, there is no set requirements for a minimum or maximum amount of whitespace allowed. In general, though, use whitespace to distinguish different functions and different logical statements (if/else, try/catch, switch, etc.) However, this is up to the discretion of the TSLA working on the code. Try not to go crazy, though.
	
# Functions
	
All functions should use camelCase in the way that they are declared.  Ensure that the name of the function ACTUALLY describes what said function does when it is called upon.

	function stormtrooperAim() {
    	//
    	// Some code here
  		//
	}

# If/Else Statements

Generally, it is acceptable to use the short version of an if statement only if the code that precedes it is short (~ 30 characters). Otherwise, use the regular version of an if statement. This version will require that the opening curly brace be on the same line as the if statement. Else statements will go on the line after the closing brace.

	function lightsaberColor(name, movie){
    	var lightsaberColor
    	if(name == "Obiwan") lightsaberColor = "blue" //use when code is short
   		else if(name == "Luke"){
       	 	lightsaberColor = "blue" //use when code is longer/has conditions inside statement
        	if(movie == "Return of the Jedi"){
                lightsaberColor = "green"
        	}
    	}
    	return lightsaberColor
	}

# JavaScript

When you require JavaScript in your file, it should be at the bottom of the file. The reason for this is because then the browser will parse the HTML first, which can lead to quicker load times.
	
# PHP

When you require PHP in your file, it should be at the top of the file (As long as it is within reason). 
	
# HTML

Element names should be in all lowercase. It is required that you close all elements after you are done with it. Data attribute names should also be using camelCase, and the values for the attributes should be quoted.  When working to import CSS and JavaScript files into the HTML file, it is not required that you place them in the /<head/> of the document. 
	
# Database 

When working with the SQL databases that we have, there are several important rules that you must follow. When you create a table, the name of said table should be plural (i.e. Employees rather than Employee). All data in the tables should be normalized, which means that the data should be separated (i.e. not having a name field for “Peter Boilermaker”). Data redundancy is not allowed, so do not put the same data twice in the database. Foreign key constraints on tables in the database are allowed. When naming the tables, use BigCamelCase, but when you are naming columns, use camelCase. 
	
