At the time of writing, we currently have two servers that we are running from. These are ITCR-OPS and our new WebServices Instance. The eventual plan is to move everything to the WebServices server, but we haven't gotten that far yet. The process for accessing files is generally the same between the two servers, though.

## ITCR-OPS Information

- Hostname: **itcrops.itap.purdue.edu**
- Username: **oszones**
- Password: Can be found on the passwords page of the operations wiki

!!! danger
    The only place you should be changing files here is under the directory `/var/www/html/tools/`. Everything else on this server is not used by ITaP Labs.

!!! note
    This server is a UNIX environment (as you may have noticed from the directory structure). You can also access it via SSH to get a terminal that will allow you to do more things. This can be done via PuTTY, which is a pre-installed application. You'll use the same credentials to log in, but you'll have to go to `Connection -> SSH -> Kex` and move `Diffie-Hellman group 14` to the top of the list before logging in.

    Personally, I prefer to use Git Bash (which should already be installed) because it comes with a normal SSH client that feels more like what you'd get in a UNIX terminal. PuTTY is just kinda clunky.

## WebServices Information

- Hostname: **ldvwebapa02.www.purdue.edu**
- Username: Your career account username
- Password: Your career account password (not BoilerKey)

!!! danger
    In order to be able to access the WebServices Server, you'll have to request that an account be created for you. This can take some time to process, so go ahead and do it as soon as possible. The request form for getting a WebServices Unix account can be found [here](https://www.purdue.edu/itap/webservices/forms/secure/account_form.php).

    The Following are the options you should choose/fill out on this form:

    - Type of Account: **Individual**
    - Project Team/Department: **ITaP Labs**
    - Supervisor Email: **lfrench@purdue.edu**
    - Select the server(s) on which to create the account(s): **ldvwebapa02 (Shared Apache HTTPD - aka PPWC 2017)**
    - Web site URL: **https://www.purdue.edu/itap/itpm-ls/**

    All the other options can be left blank or default, based on their initial value.

!!! tip
    When your account is created on the server, your home directory will contain links to `/var/www/html/root/` and `/var/www/data/root/` (named HTML and DATA). I found it very useful to create a link that goes directly to the itpm-ls folder. To do this, right-click in your home directory and choose `New -> Link`. Then, under `Link/shortcut file` put a name for the link and under `Point link/shortcut to` put `/var/www/html/root/www.purdue.edu/itpm/itpm-ls/` and you'll have a quick shortcut to our server.

!!! note
    This server contains the data for most of the sites under the purdue.edu domain. However, we are only able to write to files within the directory `/var/www/html/root/www.purdue.edu/itap/itpm-ls` and its counterpart `/var/www/data/root/www.purdue.edu/itap/itpm-ls`.

!!! note
    Please do your best to keep code as structured and clean as possible in this server. This means putting JavaScript and PHP into functions files rather than inside the page source, as well as using a stylesheet for your CSS instead of using a bunch of inline or embedded CSS. For more information on coding standards and best practices, look [here](#coding-standards).

## Getting Onto the Server

It is possible to access the server using any FTP client that supports the SFTP protocol, but most of us here use [WinSCP](https://winscp.net/eng/download.php). We have tried installing it on the computers within the office and it functioned properly. However, none of the settings (sites, layout, default editor) can be saved if it is installed in this way. Because of this, you'll want to install WinSCP manually onto your home directory.

Once you're in WinSCP, just enter the credentials listed above in the window that appears. The protocol should be "SFTP" and port "22" for both servers. You can click "Save" if you don't want to have to enter these credentials every time you login. Just click the "Login" button and you should be able to access the server's filesystem.

