# API Introduction

This one's the big kahuna, the Labs API. Before we get into any specific details, I'd like to give you a quick overview of the Labs API and how it works.

The Labs API is a (mostly) RESTful API. REST stands for "Representational State Transfer" which is a sort of design philosophy for building APIs. The basic idea behind this is that the API is stateless. Essentially, what "stateless" means for an API is that previous interactions that the API has had with clients do not affect the way it handles requests. This means that the API does not remember anything from your previous requests (that would affect the outcome of future requests) and it allows the service to be extremely simple but powerful, as well as scalable.

## API Security and Access Tokens

Because the API has no memory of who you are or what requests you've made in the past, we use a system of "access tokens" that allow us to authenticate users and ensure they are only getting access to data they should be able to access. These "access tokens" are just strings that are randomly generated and are tied to a user and specific permission sets.

The access tokens for the API consist of "LABS-" followed by 64 randomly generated letters and numbers. These access tokens should be included in every request so that the API can determine whether the request should be permitted. For more information on access tokens and authorization, see the [token documentation](labs_api.md#api-authentication-and-authorization).

!!! note
    If you're using the pre-built JavaScript APIClient wrapper, you won't need to worry about including the access token, as it will automatically be included in every request

# Making requests to the API

The Labs API uses the hypertext transfer protocol (HTTP) for making requests and sending responses. This is the same protocol that your web browser uses to load your favorite websites like Reddit. MDN has a great explanation of [HTTP and its basic principles](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview) that I highly reccommend you check out if you're not already familiar with HTTP.

Each HTTP message consists of headers and a body. The headers in an HTTP message define important things, such as where a message should be sent, what version of the protocol it uses, what type of data it's sending, and much more. The body of the request contains the actual data that we're trying to send/receive and it is where you'll get all your data.

The body of a message is just plain-old bytes of data. However, you can set the headers of the message to tell the recipient how to interpret the data you've sent it. This could be a video file, an image, some music, a bit string, plaintext, etc.. **The Labs API encodes all its responses in JSON format and expects requests to be JSON as well**.

In order to make a request to the API, you'll first have to figure out what endpoint you need to request from. An endpoint is just a term that we use for a URL that can be used to make a specific request. Almost all of the endpoints for the API can be found in the `/itpm/itpm-ls/labs/api/` directory, and each one of them relates to a specific resource. The process for using these endpoints will remain mostly the same, but there is documentation for each separate endpoint in the [API Reference](api_reference.md).

Once you know your endpoint, you'll need to send it an HTTP request using the proper method for what you wish to accomplish. The API supports 4 different [HTTP verbs](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods):

HTTP Verb | What it's used for
--------- | ------------------
GET | Listing records and grabbing a specific record
POST | Creating new records
PUT | Modifying existing records
DELETE | Deleting existing records

Finally, you'll need to provide the proper parameters for your request. These are defined in each resource's documentation. Below is a simple example of a request that will list out the first 10 machines it finds (sent to the `machine` endpoint).

```html
<script type="text/javascript" src="APIClient/APIClient.js"></script>
<script>
  //All examples from here on out will assume you already have an APIClient object called "api"
  var api = new APIClient();

  //All examples from here on out will also assume you are surrounding all calls in try/catch statements, as making requests can cause errors to be thrown.
  try {
    var respMachines = await api.get('machine', {
      limit: 10
    });

    //respMachines looks like this:
    /*
      {
        ok: true,
        page: 0,
        machines: [
          {
            "station_name": "1408crtn1011s1.itap.purdue.edu",
            "station_type": "SOLSTICE",
            "mac_addr": "58:fc:db:40:a1:68",
            "ip_addr": "10.161.1.236",
            "last_online": "2019-07-15 13:10:20",
            "online": "1",
            "ignore_station": "0",
            "location": "",
            "last_cleaned": "2018-12-01 12:00:00",
            "ignore_clean": "1",
            "room": ""
          },
          ...
        ]
      }
    */
  } catch (error) {
    console.error(error);
  }
</script>
```

!!! note
    Any examples that use JavaScript in the Labs API section or API Reference of this documentation will assume the Labs APIClient wrapper is being used.

!!! note
    Note that GET requests to the API do not use a request body. This is because (while it is not strictly against HTTP standards), most libraries do not support sending a body inside a GET request, including AJAX. Because of this, we must send our data in the query string of our request.

    This isn't something we must worry about when using the APIClient, because it accounts for this and will turn the data you give it into a query string for GET requests.

# Filtering with GET

There are a few different features built into the GET request that allow you to retrieve a listing of records and filter it:

Option | Type | Description | Default
------ | ---- | ----------- | -------
_limit | integer | Specifies the number of items to list. If there are less records in the database than this number, only the existing amount will be listed. If this number is larger than the specified `max_list` for a resource, it will be capped at `max_list`. If this is omitted, the resource's `default_list` will be subsituted. | `default_list` of resource
_sort_by | string | Specifies the field to sort the list by. This must be a field that exists in the resource's `fields` array. If ommitted, the list will be sorted by the resource's identifer. | `identifier` of resource
_reverse_sort | boolean | Specifies whether to reverse the sort direction. By default, items are sorted in ascending order. If this is set to true, they will be sorted in descending order. | false
_filter | string | Allows complex filtering. Can use a range of different operators to determine what you want. | (user_auth <= 1 &#124;&#124; user_auth > 3) && user_id != "login"

**_filter parameter operators:**

Operator | Description | Example
-------- | ----------- | -------
= | Equal | user_id = 'peter_boilermaker'
!= | Not Equal | user_id != 'peter_boilermaker'
&gt; | Greater than | att_points &gt; 9
&gt;= | Greater Than or Equal To | att_points &gt;= 10
&lt; | Less Than | att_points &lt; 9
&lt;= | Less Than or Equal To | att_points &lt;= 10
&#124;&#124; | Or | att_points > 10 &#124;&#124; user_id = 'pfmartin'
&amp;&amp; | And | att_points > 10 &amp;&amp; user_id = 'pfmartin'
before | Before (synonym for &lt;) | time_start before '2019-01-01 10:00:00'
after | After (synonym for &gt;) | time_start after '2019-01-01 10:00:00'
() | Parentesis can be used for grouping conditions | See above

# API Authentication and Authorization

## What are Access Tokens?

To a user, an access token for the Labs API is nothing more than a string. This string allows them to make requests to the API and get information. However, on the backend, an access token represents more than just a string.

The access token is connected to a specific user, as well as different permissions that we refer to as "scopes". Go ahead and have a look at the table structure for APIScopes, APITokens, and APITokenScopes in the database and you might get a basic idea of how these work.

Essentially, an access token is randomly generated when a user requests one. That token is assigned an incremental TokenId, an Expiration date-time (which is exactly one day after it is created, in most cases), and is linked to a user_id.

This token by itself doesn't do much. However, there is a table called 'APITokenScopes' that links an access token to each "scope" that it is allowed to use. These scopes define whether or not a user will be able to use a specific action in the API. More information about scopes will be given below.

The access token is given to the user and should then be included in every request the user makes to the API, so that it can determine if that request will be allowed to continue, based on the token's scopes.

## Access Token Request Flow

There is a specific process that a user must take in order to gain an access token. This process ensures that the identity of the user is valid and that the token they are assigned only uses the scopes they are allowed to use.

### Step 1: Authorization and temp_key

The first step in the token request process is to authenticate the user. This is accomplished by redirecting your user to the `/api/auth/cas_auth.php page` in their browser with the query parameter 'redirect'. This page will automatically apply CAS authentication to ensure that the user is who they say they are. Once the user has logged in to CAS, the page will display a button that the user must press. When This button is pressed, the page will redirect them to the url given in the 'redirect' query parameter, adding its own 'temp_key' parameter that contains the temporary key that can be exchanged for an access token. **This temp_key expires 60 seconds after creation, so be sure to exchange it immediately**.

Example URL to redirect to:

`https://www.purdue.edu/itap/itpm-ls/labs/api/cas_auth.php?redirect=https://example.com/page/to/redirect/to`

The URL the user will be sent to after authentication:

`https://example.com/page/to/redirect/to?temp_key=TEMP-XXXXXXXXXX`

### Step 2: temp_key -> Access Token Exchange

The second step in the token request process is to exchange the temp_key that was retrieved in step 1 for an access token to the API. This temp_key is tied to the user_id of the person who requested it, and must be exchanged for an access token linked to the same user. To exchange the key for an access token, make a POST request to `/api/auth/get_token.php`. Below is an example of the request that should be made.

```html
<script>
  var response = await axios.post("https://www.purdue.edu/itap/itpm-ls/labs/api/auth/get_token.php", {
    scopes: ["available"],
    temp_key: "TEMP-XXXXXXXXXX...",
    alias: "purduepete"
  });
</script>
```

```html
<script>
  console.log(response);

  /*
    {
      ok: true,
      token: "LABS-XXXXXXXXXXXX"
    }
  */
</script>
```

The `scopes` parameter should be an array of all the scope names you wish for the access token to have. These scopes will be checked against the user to ensure they are permitted.

You'll notice that in this example, the only scope we requested was `available`. This is a special macro that will assign every scope that is available to the user when the token is created. Instead of using this, you can also explicitly define the scopes you wish to use.

!!! danger "Important Note"
    For any special tokens that are created (which usually don't have an expiration date), it is required that the scopes given to the token are named explicitly. In addition, the token should only be linked to the scopes that it actually needs. **Do not give permanent tokens access to every scope**. This is a major security risk and would be a violation of FERPA/GLBA guidelines, which can cost the university tons of money and could definitely get you fired.

After completing this process, you should have your access token! It's a good idea to store it somewhere. Cookies are especially good for this, because you can set the expiration on the cookie and you'll know when the token has expired. Some purpose-built tokens may not have an expiration date, but you'll know if they don't, because you'll have to create them manually.

# Scopes

If you've ever worked with Google's OAuth authentication process, you may be familiar with the idea of a scope. Essentially, a scope is just a string that defines a specific action that may be taken. The basic anatomy of a scope in the LabsAPI consists of `[resource].[action].[specifier]`:

Section | Description
------- | -----------
resource | This is the name of the resource that the action applies to
action | This is the HTTP request method that you're using (GET, POST, PUT, DELETE)
specifier | This defines a specific type of action in relation to the request being made. For example, a `specifier` of `self` means the user has access to this `resource` and `action` only for records that are linked to themself.

**Specifiers:**
Specifier | Description
--------- | -----------
all | This specifier allows all requests to this scope's resource when made via this scope's action.
self | This specifier allows requests to this scope's resource via this scope's action only if the resource has a defined "self field" and the record(s) that is/are being accessed in the request are linked to the user who owns the token with this scope.
others | This specifier allows requests to this scope's resource via this scope's action only if the resource has a defined "self field" and allows access to records that are linked to users other than the user who owns the token with this scope.

Some examples of scopes:

* `employee.GET.self` - Will allow the token to retrieve their own record, but not those of others
* `employee.PUT.others` - Will allow the token to modify the records of other employees.
* `machine.POST.all` - Will allow the token to create new records for the `machine` resource.

# The API Client

I have created a JavaScript class called APIClient that makes interactions with the API via JavaScript trivial. Of course, if you prefer to use some other library or even create your own implementation of the API client, this is possible. The API is designed to be extremely flexible and can be accessed from anywhere in the world and on any kind of system, as long as it has basic networking.

The APIClient class can be found in `/labs/dashboard/APIClient/APIClient.js` and is essentially a wrapper around the Axios library. Because of this, you'll need to include Axios to your project before including the `APIClient.js` file.

Axios is a promise-based HTTP library for JavaScript. If you are not familiar with `Promise`s in JavaScript, I recommend you read up about them. [Here](https://www.geeksforgeeks.org/javascript-promises/) is a pretty good explanation of Promises. Additionally, the APIClient makes heavy use of a new syntax introduced to JavaScript in ES6 called [async/await](https://javascript.info/async-await), which you should read up on if you aren't familiar with it.

If you're not working with the APIClient within the Dashboard, you'll have make some modifications to the token grabbing process in order to get tokens properly. Once the APIClient has been initialized, it will check to see if there is a cookie containing a valid token. If it can't find one, it will run the process to get a new token, then store it.

Either way, once the API has fetched an Access Token, it is ready to start making requests. On the dashboard, a request will be made to grab information about the employee who is logged in. The APIClient emits an event every time a request finishes. These events are `ApiGET`, `ApiPOST`, `ApiPUT`, and `ApiDELETE` respectively. Event listeners attached to these events will receive the name of the `resource` that the request was made for and the data that was returned. Have a look at the function in [this section](dashboard.md#the-new-dashboard) to get an idea of how to utilize this.

Most of the time, you'll just want to use the `await` keyword and save the response of the request to the API into a variable. The example below shows how this can be accomplished.

```html
<script>
  var api = new APIClient();

  var response = api.get('employee', {
    PUID: "0123456789"
  });

  console.log(response);

  /*
    response: {
      ok: true,
      employee: {
        PUID: "0123456789",
        user_id: "peter_boilermaker",
        fname: "Peter",
        lname: "Boilermaker",
        ...
      }
    }
  */
</script>
```

Remember that each `Resource` has an `Identifier` and the `Identifier` for employees is `PUID`. Any non-listing GET request you make will require you to specify the unique idenifier that relates to the record you wish to find. This is the same for PUT and DELETE requests.

For each of the four request types, there is an api function: `api.get(), api.post(), api.put(), api.delete()`.

The parameters you'll pass into these functions are the same for each:

`api.[method](resource, data);`

Parameter | Type | Description
--------- | ---- | -----------
resource | string | The name of the resource you're looking for (ex: "employee")
data | object | An object containing the data you want to send with your request. This will differ based on what you're trying to do and the resource you're using it on.

# Error Handling

Each call to the API has the possibility to return errors. This is why it is recommended that every call to the API be wrapped in a try/catch statement.

The API does a very good job of teling you exactly what went wrong and/or what was missing from your request. The APIClient JavaScript wrapper will automatically log the server's response in the case that an error takes place. This means that if something goes wrong, you should just need to look at your console to find out what happened.

In some rare cases, the API will not send its response properly if an error takes place. If you find this happening, it means there is a bug in the API that needs to be fixed.

Every single response that the API spits out has a field called `ok`. This field is a boolean and does exactly what it says. If the request was processed successfully and there was nothing wrong, `ok` will be `true`. If there is ever an error, `ok` will be `false`.

In addition, if `ok` is ever `false`, there will be another field in the response called `error`. This response is a string that explains what was wrong with the request. Sometimes there will be additional fields if more data is required to fully describe the error.

Using a combination of `ok`, `error`, and the HTTP Response code returned by the API, you should be able to successfully debug any problems you have!

Below, you can see an example of what the response looks like if an error is returned.

```html
<script>
  var response = await api.get('employee', {
    PUID: "0123456789"
  });
</script>
```

```html
<script>
  console.log(response);

  /*
    response: {
      ok: false,
      error: "Resource of type 'employee' with given 'PUID' could not be found.",
      request_PUID: "0123456789"
    }
  */
</script>
```

