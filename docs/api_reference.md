# API Reference

Originally, this was going to be a list of static web pages giving all the information about each API Resource. However, it has become clear to me that this would not be a good solution to the problem, as it would be easy for the information on these pages to get out of date very quickly.

One of the more recent updates to the Labs API changed a lot of the way the API is handled internally. Before this change, each resource had its own php file with a class describing the resource. This meant that all the values for things were hardcoded into a PHP file, and that we had to edit and deploy these files any time a new resource was made.

With the new version of the API, everything is handled dynamically! Unless you're making changes to the API itself and/or the way it works, you should never have to edit the files in the API repository. Instead, all information about resources is handled dynamically in the database.

Instead of listing out every field and property for each API Resource, this page will tell you how to go in and look at what's happening with the resources. I feel that this method of referencing things is better, because the database will always contain the current information about API Resources and will never be out of date.

# List of Resources

If you want to see a listing of all the resources that are available to you, head over to phpMyAdmin and look at the APIResourceData table. This table should contain one row for each unique resource available through the API. This table is where any static data about an API Resource is contained. It gives you the name of the resource, the name of the table for the resource, and more information. See below for a complete description of this table.

# API Examples (JavaScript)

The API has four main methods, GET, POST, PUT, and DELETE. 

Method | Use Case
------ |  --------
GET | Retrieves information from table
DELETE | Deletes entry from table
POST | Adds NEW entry to table
PUT | Edits an existing entry, however if the entry doesn't already exist, PUT will act as POST and create a new entry. 

Examples of each entry will be provided below. They don't correspond to any existing table, you cannot just copy and paste them. You need to edit each individual call depending on what table you want to work with and which entries. 
API calls reference the table by its snake_case non-plural name, which can be found in the API Resources table in MyPHPAdmin.
## API GET: 
Note with GET, you do not have to provide a value for each column. You can use this in combination with filtering to your advantage to narrow down which values of the table get returned by the API. 
```
async function getTester() {
    specificValue = 'project1'; //this will be defined by your own script
    filterText = 'snake_case_resource_value = ' + String(project1); 
    try {
        respGetTester = await api.get('snake_case_resource_name', { //Table resource name is written in non-plural snake_case
            _limit:500,
            _filter:filterText
        };
    }
    catch(error)
        console.error(error);
};
```
## API DELETE: 
With DELETE, you must provide all values corresponding to the entry to be deleted. It is often useful to perform an API GET before a delete to get all of these values 
For a table with columns value1, value2, and value3:
```
async function deleteTester() {
    tableValue1 = xxx;
    tableValue2 = yyy;
    tableValue3 = zzz;
    try {
        respDeleteTester = await api.delete('snake_case_resource_name', {
            value1: tableValue1,
            value2: tableValue2,
            value3: tableValue3
        });
    } catch(error) {
        console.error(error);
    };
};
```
## API POST:
Like delete, you must provide all values relevant to the resource (except for auto-incrementing ids, which is an option set in MyPHPAdmin for a specific column) to create a new entry.
For a table with values value1, value2, and value3
```
async function postTester() {
    newValue1 = xxx;
    newValue2 = yyy;
    newValue3 = zzz;
    try {
        respPostTester = await api.post('snake_case_resource_name', {
            value1: newValue1,
            value2: newValue2,
            value3: newValue3
        });
    } catch(error) {
        console.error(error);
    }
}
```
## API PUT:
Like delete, you have to provide all columns in a table including auto-incremented ids in order to perform a PUT
For a table with values value1, value2, value3
```
async function putTester() {
    updatedValue1 = xxx;
    updatedValue2 = yyy;
    updatedValue3 = zzz;
    try {
        respPutTester = await api.put('snake_case_resource_name', {
            value1: updatedValue1,
            value2: updatedValue2,
            value3: updatedValue3
        });
    } catch(error) {
        console.error(error);
    }
}
```

# Finding all information for a resource

## APIResourceData
As mentioned above, all of the static information for each resource is stored in the APIResourceData table. See below for a description of this table.

## Resource Table Infomration
In addition, more information about a resource is grabbed dynamically by the API depending on the structure of the table associated with the resource.

Each column in the resource's table will become a field accessible via the API. For example, the Employees table has a column named "user_id". This column will be returned as one of the fields in each record returned from a get request, because it is a column in the table. I'm not quite sure how to explain it more, so imagine you've got a variable called `employee` that came from a get request to the 'Employee' resource. With this employee variable, you should be able to access `employee.user_id`, because this is one of the columns in the table.

The API also automatically checks the types of variable passed into it to make sure they're correct. The API does a conversion from php types to mysql types internally, so below I'll list those conversions. The type of a variable in the API depends on the Type that is defined for the relevant column in the database. For example, employee.user_id will be a string, because the Employees.user_id column in the database has a type of VARCHAR. Below is a table showing how PHP types map to mySQL types:

PHP Type | mySQL Types
-------- | -----------
boolean | BOOLEAN, TINYINT, BIT
integer | SMALLINT, MEDIUMINT, INT, BIGINT
double | DECIMAL, FLOAT, DOUBLE, REAL
string | CHAR, VARCHAR, TINYTEXT, TEXT, MEDIUMTEXT, LONGTEXT, TINYBLOB, BLOB, MEDIUMBLOB, LONGBLOB, ENUM, SET, DATE, DATETIME, TIMESTAMP, TIME, YEAR

## Maximum size

The API also automatically checks the sizes of fields, as well as the formats of some fields, depending on their type. Below, you can find a table listing all the default maximum values for each type:

mySQL Type | Default Maximum
---------- | ---------------
TINYINT | 127
SMALLINT | 32767
MEDIUMINT | 8388607
INT | PHP_INT_MAX
BIGINT | PHP_INT_MAX
DECIMAL | 9999999999
FLOAT | PHP_INT_MAX
DOUBLE | PHP_INT_MAX
REAL | PHP_INT_MAX
BIT | 1
BOOLEAN | 1
SERIAL | PHP_INT_MAX
DATE | 10
DATETIME | 19
TIMESTAMP | 19
TIME | 8
YEAR | 4
CHAR | 1
VARCHAR | 1
TINYTEXT | 255
TEXT | 65535
MEDIUMTEXT | 16777215
LONGTEXT | PHP_INT_MAX
BINARY | 1
VARBINARY | 1
TINYBLOB | 255
MEDIUMBLOB | 16777215
BLOB | 65535
LONGBLOB | PHP_INT_MAX
ENUM | PHP_INT_MAX
SET | PHP_INT_MAX

### Strings
If the type of the column converts to a string (check table above), the maximum size of the column relates to the length of the string. The maximum length for a string type can be defined in the column definition under the LENGTH/VALUES section.

### Integers
If the type of the column converts to an integer (check table above), the maximum size of the column relates to the value of the integer. The maximum integer size will always be set
to its default, based on the mySQL Type of the column. See above table for these defaults.

### Date/Time
mySQL types related to date/time (DATE, DATETIME, TIMESTAMP, TIME, YEAR) are a special case, in that they each require a specific format to be met. If this format is not met, the API will return an error. Below are the formats required for each of these types:

mySQL Type | Format
---------- | ------
DATE | `YYYY-MM-DD`
DATETIME | `YYYY-MM-DD HH:mm:ss`
TIMESTAMP | `YYYY-MM-DD HH:mm:ss`
TIME | `HH:mm:ss`
YEAR | `YYYY`

!!! note "Above date/time formats"
    The formats given in the above table adhere to the rules for date/time formats defined by moment.js. This is because the moment.js is by FAR the most used method of handling time objects within the dashboard, and the rest of our tools.

## Identifier
As you probably know by now, each API Resource has a field that's labeled as its "identifier". The identifier for a table is always its primary key. Note that currently, the API is unable to support table structures with compound primary keys (more than one column used for primary key). If your table needs this, you can generally get around it by creating an auto-incrementing column and making that the primary key instead.

## PUT Options
With the new verison of the API, fields that can be used in a PUT request now include every column in the resource table, EXCEPT THE IDENTIFIER. The identifier can never be changed with a PUT request. If you need to change the identifier of a record, you'll have to delete the record and create a new one.

## POST Required and Optional
When creating a new record with a POST request, the API will automatically determine which fields must be provided and which fields are optional.

Auto-incrementing primary keys are always optional parameters, because the value is set automatically by the database. Apart from those, the API determines if a field is required based on whether the column has a default value, as well as whether the column can be null. If the column has a default value defined, the field will be optional. If the column allows null values, the field will be optional. Otherwise, the field is required.

In other words, if the column has a default value, or the column allows null values, the field is optional. If the column has no default value and the column does not allow null values, the field will be required.

## Self Fields
Generally, any column that contains a user_id ("pboilermaker") is considered a "self field". These fields are necessary for permissions. For example, an LA should be able to access information about their own employee record, but not the records of other employees. So, the field "user_id" in the Employees table is a "self field".

Self fields are used in permission checks to determine if an action will be allowed. They go hand-in-hand with the permission scopes for the related resource. If a resource has a "self field", it should also have additional scopes with "self" and "others" as the specifier for each type of request. Take a look at the permission scopes for Employee as an example.

An example to illustrate how self fields work: The Employee resource has a self field "user_id". For GET requests, there are 3 permission scopes: Employee.GET.all, Employee.GET.self, and Employee.GET.others. So, when a GET request is made to the Employee resource, the API will first check to see if the token used has access to the "Employee.GET.all" scope. If so, the request will be allowed. If not, the API will then check to see if the value of "user_id" in the request is equal to the user that owns the token. If so, the API will check for the Employee.GET.self scope, and if not the API will check for the Employee.GET.others scope.

**Self fields are defined in the Comments field for a column.** If a column should be a self field, you must set its Comments field to be "self". I understand that this is kind of a strange way to handle this, but it's the best I could come up with since I wanted everything to be dynamic. Look at Employees.user_id for an example of this.

## Scopes
Every API Resource should have scopes associated with it. Scopes can be found in the APIScopes table. Each resource should have the following scopes:

- [ResourceName].GET.all
- [ResourceName].POST.all
- [ResourceName].PUT.all
- [ResourceName].DELETE.all

Additionally, if the resource contains a self field, it should have the following scopes:

- [ResourceName].GET.self
- [ResourceName].GET.others
- [ResourceName].POST.self
- [ResourceName].POST.others
- [ResourceName].PUT.self
- [ResourceName].PUT.others
- [ResourceName].DELETE.self
- [ResourceName].DELETE.others

More information about scopes can be found in the "Labs API Documentation" section of these docs.

# Creating a New Resource
This section provides all of the steps required to create a new resource with the API.

## Resource Definition
Every resource must have a row in the APIResourceData table defining it. See the next section for a description of the fields in this table.

## Table formatting
Read the above section for information on how the structure of your table will affect its interactions with the API.

## Scopes
You'll need to add the following scopes to the APIScopes table for your resource:

- [ResourceName].GET.all
- [ResourceName].POST.all
- [ResourceName].PUT.all
- [ResourceName].DELETE.all

Additionally, if your resource contains a self field, you'll need the following scopes:

- [ResourceName].GET.self
- [ResourceName].GET.others
- [ResourceName].POST.self
- [ResourceName].POST.others
- [ResourceName].PUT.self
- [ResourceName].PUT.others
- [ResourceName].DELETE.self
- [ResourceName].DELETE.others

!!! tip "API Scope Format"
    Whenever I refer to API Scopes in this documentation, I generally use the format [Resource].[Action].[Specifier]. Here's a more detailed description of this format: [ResourceName].[GET|POST|PUT|DELETE].[all|self|others]

!!! tip "New Scopes and Tokens"
    It is important to note that when new scopes are added to the APIScopes table, they are not automatically added to existing tokens. In order to access these scopes, you'll need to get a new token. This can be done simply by either clearing your cookies or deleting your token manually from the database, then reloading the Labs Dashboard.

# API Table Descriptions
Below are full descriptions of the tables the API uses.

## APIResourceData
This table contains the definitions for all the different API resources. Generally, a table in the database = one resource. However, you still have to put an entry in this table for the API to recognize the resource.

Column | Description | Formatting
------ | ----------- | ----------
ResourceName | The name of the resource. This is only really used internally. | Resource names should be BigCamelCase and should **ALWAYS BE SINGULAR** (i.e.: Employee vs Employees)
TableName | The name of the table relating to the resource | This should match the table name exactly. Table names should always be BigCamelCase and should be plural (i.e.: Employees vs Employee)
SnakeName | The name of the resource, but in snake_case. This is the value that you put in the first parameter of api.get(), api.post(), api.put(), api.delete() | Should be all lowercase with underscores between words. Additionally, this field should always be in singular form
SnakeNamePlural | The same as SnakeName, but in plural form. This is what the API names the array containing all the returned records in a GET request. | Should be all lowercase with underscores between words. Additionally, this field should always be in pluaral form
DefaultListAmount | The amount of record that will be returned from a GET request if _limit is not specified | A number
MaxListAmount | The maximum amount of records that can be returned in one request, regardless of the _limit supplied | A number

## APITokens
This table contains all the active API Tokens. Note that the scopes attached to tokens are stored in APITokenScopes.

Column | Description | Formatting
------ | ----------- | ----------
TokenId | Auto-incrementing unique ID for each token | A number
user_id | The user_id of the person to which this token belongs | Should be a valid user_id found in Employees table
Token | The actual token secret string that is used for authorization | Starts with "LABS-" followed by 64 randomly generated letters and numbers
Expiration | The date and time this token expires | YYYY-MM-DD HH:mm:ss

## APITempKeys
This table contains all the active temporary keys. These keys only last 60 seconds, and are used to obtain a new token after authenticating.

Column | Description | Formatting
------ | ----------- | ----------
TempKeyId | Auto-incrementing unique ID for each token | A number
user_id | The user_id of the person to which this token belongs | Should be a valid user_id found in Employees table
TempKey | The actual temporary key secret string that is used for getting a token | Starts with "TEMP-" followed by 64 randomly generated letters and numbers
Expiration | The date and time this temporary key expires | YYYY-MM-DD HH:mm:ss

## APIScopes
This table defines "scopes", which are like permission nodes for each resource.

Column | Description | Formatting
------ | ----------- | ----------
ScopeId | Auto-incrementing unique ID for each scope | A number
Resource | The ResourceName of the resource to which this scope belongs | Same restrictions as APIResourceData.ResourceName
Action | The type of request (GET, POST, PUT, DELETE) | Must be one of the following: "GET", "POST", "PUT", "DELETE"
Specifier | The specifier describing what sort of action this scope allows, in relation to the given resource and action | Must be one of the following: "all", "self", "others"
ReqAuth | The required Employees.auth_level for an employee to be able to request a token containing this scope | Number >= 0

## APITokenScopes
This table links scopes to tokens. A record in here means that the given token has access to the given scope.

Column | Description | Formatting
------ | ----------- | ----------
TokenId | The APITokens.TokenId of the relevant access token | Should be a valid TokenId found in the APITokens table
ScopeId | The APIScopes.ScopeId of the relevant permission scope | Should be a valid ScopeId found in the APIScopes table

## APIUserScopes
This table allows us to give specific employees access to specific permission scopes, regardless of the ReqAuth of that permission scope. Entries in this table only mean that the employee can request a token with the given scope, not that their token has that scope.

Column | Description | Formatting
------ | ----------- | ----------
user_id | The Employees.user_id of the employee in question | Should be a valid user_id found in the Employees table
ScopeId | The APIScopes.ScopeId of the permission scope in question | Should be a valid ScopeId found in the APIScopes table

## APILog
This table keeps records of every request that is made to the API. This data is useful for creating metrics on what parts of the API are used most, where the requests are coming from, and also helps us debug things.

Column | Description | Formatting
------ | ----------- | ----------
id | Auto-incrementing unique ID for each log entry | A number
origin_url | The URL the request came from. This value comes from the _api_url field passed with the request. If the request does not provide this information, it is set to 'unknown' | A URL
resource | The name of the resource for which the request was made | A valid ResourceName from the APIResourceData table
request_type | The HTTP type of the request | GET, POST, PUT, or DELETE
user_id | The user_id of the employee who made the request. This is used instead of the TokenId, because tokens are deleted after they expire. | A valid user_id found in the Employees table
timestamp | The date and time that the request was made | YYYY-MM-DD HH:mm:ss
request_ip | The IP that the request came from | A valid IP