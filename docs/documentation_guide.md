# Documentation Guide

## PHP Documentation

```php
<?php
  /**
   * Adds two integers and returns their sum.
   * 
   * @param int $first_num The first number to add
   * @param int $second_num The second number to add
   * @return int The sumer of $first_num and $second_num
   */
  function sum($first_num, $second_num) {
    return $first_num + $second_num;
  }
?>
```

For PHP, we use a documentation engine known as PHPDocumentor. This uses "docblocks" for documenting functions, classes, files, etc. These docblocks can actually be turned into automatically generated documentation websites. However, we don't use that functionality, as having documentation here works well enough. We do still require that all of your PHP classes, functions, etc. have docblocks on them, so that we have some sort of standardized code documentation outside of whatever you may add to this site. For more information about PHPDocumentor and docblocks, look [here](http://docs.phpdoc.org/guides/docblocks.html).

## HTML Documentation

```html
<!-- This is a comment in HTML -->
<!-- They can be useful for marking sections in your markup -->
```

HTML does have a way of writing comments `<!-- ... --> However, these are nto ver readable and when you try to put too many comments in an HTML page, it just makes it hard to read and see what's going on. Instead, it's better to just always indent your HTML properly and try to write ids and class names that make sense. Honestly, because HTML is a markup language and not a programming language, it is much easier to read and honestly, well-written HTML should not need documentation.

## JavaScript Documentation

```html
<script>
  /**
   * Adds two numbers and returns their sum.
   *
   * @param {number} first_num - The first number to add
   * @param {number} second_num - The second number to add
   * @returns {number} - The sum of first_num and second_num
   */
  function sum(first_num, second_num) {
    return first_num + second_num;
  }
</script>
```

For documenting JavaScript (which is what most of your code should be), we use a library called [JSDoc](https://devdocs.io/jsdoc/) that is very similar to PHPDoc. It will also automatically generate documentation webpages that we can look through, though we don't really use that functionality. The nice part about JSDoc is that it can integrate with IDE's to show you information about a function, class, or variable while you're writing your code.

The biggest part of making your code easy to read and well-documented is to structure your code properly. This means separating code into functions with good names, as well as including docblocks for all functions. Generally, it's a good idea to put code anywhere you do something that might be complex or difficult to understand. Also, make sure you're following the guidelines outlined in the Coding Style Documentation.