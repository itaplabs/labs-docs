Oftentimes, when you're making any kind of tool or website, you'll want to have a form where you can get input from your user and do something with it. This could be someone signing up for a mailing list, searching for products on amazon, signing up for an account on Spotify, etc. Forms have many uses, so you'll probably want to know how to work with them.

# Creating a Form

All we need to create a form is a little bit of HTML (below).

```php
<form id="example_form" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <input type="text" name="username" placeholder="Username" />
    <input type="submit" name="submit" />
</form>
```

This will make a form that submits the data it gets to itself. So, when the user input information and hits the Submit button, the browser will send a request with the form data to whatever page is given in the form's `action` parameter. Using `$_SERVER['PHP_SELF']` just tells it that we want it to send the form data to this page. You can have it submit the data to any page, but it's usually easiest to just process the data at the top of the page with PHP and send it to itself.

The `method` attribute of the form tells the browser how to send the data the user gives it. `POST` and `GET` are both HTTP Verbs that mean different things. Generally, for the caseof forms, the main difference between the two is that `GET` sends the data by putting it as query parameters in the URL while `POST` sends the data within the HTTP Request body.

There is some more explanation of the basics of HTTP (Hypertext Transfer Protocol) down in the [API Documentation](#labs-api). For now, just think of it as being that `POST` is more secure than `GET` because `GET` shows the user's data in the URL. At the same time, you can use the query parameters that `GET` does to bookmark pages, so that can also be something to keep in mind.

# Processing Submitted Form Data with PHP

Once you've got a form on your page, you'll probably want to be able to do something with it. One option is to use PHP to process data. This is useful when you want to use it to add something to the database. Once again, I'd like to stress that the preferred method for interfacting with the database is through the [Labs API](#labs-api), but there may be some case where it isn't the best thing to use.

Depending on what method you used for submitting the form, your data will either be in the `$_POST` or `$_GET` variables. These variables are associative arrays that contain any data that was sent in the request body or query string respectively. In the case of our example above, let's say we want to take the username the user gave us and print out their token total. We'd do something like what you see below.

```php
<?php
    $db = new Database();
    $username = $_POST['username'];
    $sql = "SELECT tokens FROM lsoemployee_data WHERE user_id = '$username';";
    if($db->select_first($sql, $result)) {
        echo "Your token total is: " . $result['tokens'];
    } else {
        echo "Something went wrong when looking for your tokens. Are you sure the username was correct?";
    }
?>
```

# Processing Submitted Form Data with JavaScript

!!! note
    In cases where JavaScript can be used instead of PHP, it is HIGHLY RECOMMENDED to use JavaScript. In most cases, it should be possible and much easier to use JavaScript in conjunction with the [Labs API](labs_api.md) over PHP directly interfacing with the database.

When you just want your form data to change something on the page or if you want to use the API with your application, you'll need to handle form data in the browser with JavaScript. The way we do this is by catching the event that takes place when the user submits the form and doing something with it there. Below is an example that uses jQuery to add an event handler to our form.

```html
<script>
    $('#example_form').on('submit', function(event) {
        event.preventDefault(); //This stops the form from submitting via the normal method and by extension reloading our page
        console.log($(this).serialize()); //Will print out the form data "serialized" (put into the format of query parameters)
        //Here, we can do whatever we want with our page, knowing that this code will run when the user presses the submit button.
    });
</script>
```

!!! tip
    Remember that in order to manipulate the DOM in any way, you have to wait until it's built and ready to be accessed, including attaching event handlers. In the case of jQuery, this means placing your code inside a `$(document).ready( function() { ... } );`.

## Submitting Form Data via jQuery AJAX

jQuery natively includes a library of functions referred to as `AJAX` (Asynchronous JavaScript and XML). Essentially, AJAX allows you to perform asynchronous http requests in the background of the browser using JavaScript, only affecting what the user sees when you want it to. AJAX is the basis for other JavaScript frameworks, such as the Fetch API and Axios (which is what the API Client uses for making calls). Additionally, AJAX is what powers the dashboard.

Use of AJAX for all page changes and other requests makes it so that the page never has to refresh and we can load external webpages directly inside the content area of the Dashboard. There is an example of a basic AJAX request that requests info.html and replaces the current page body with it given below.

```html
<script>
$('#example_form').on('submit', function(event) {
    event.preventDefault(); //Stop default event handler from running.
    $.ajax({
        url: 'info.html', //Note that this URL is relative to the URL of the current page. If info.html was one level up, we'd do '../info.html'
        type: 'post', //This is the equivalent to the form method. It defines what type of request to make.
        data: $(this).serialize(), //This will send the form's data (in query parameter format) -- Can also be sent in other forms
        success: function(result) {
        //This is an anonymous function that will be run when/if the request returns successfully. The variable "result" is the body of the HTTP response.
        $("body").html(result); //This just takes the <body> tag of our page and replaces its contents with the response we got.
        //Note that this response doesn't have to be HTML. It can be form data, JSON, plain-text, etc.
        //And you can do whatever you want with the data you get back.
        }
    });
});
</script>
```

This is just a basic example and there are many more things you can do with AJAX. Also, the data returned by the request doesn't always have to be markup (HTML). It can be a few different kinds of data. By default, jQuery will attempt to determine what kind of data was received, but you can explicity tell what kind of data you expect to receive and jQuery will parse the response as that data type. For more information on the parameters to the `$.ajax` call and what values they can take on, look [here](http://api.jquery.com/jquery.ajax/).

